FROM python:latest

RUN mkdir /app
COPY ./ /app
WORKDIR /app
RUN chmod +x /app/run.sh

CMD /app/run.sh
